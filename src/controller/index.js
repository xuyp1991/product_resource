import vue from 'vue'
import vue_router from 'vue-router'
import Vuex from 'vuex'
vue.use(vue_router);
vue.use(Vuex);

import product_router from '../views/product_router.vue'
import product_router_history from '../views/product_router_history.vue'
import product_cert from '../views/product_cert.vue'
import report from '../views/report.vue'

import admin_index from '../views/admin_index.vue'
import admin_product_sku_manage from '../views/admin_product_sku_manage.vue'
import login from '../views/admin_login.vue'

import manage_admin from '../views/manage/index.vue'
import manage_admin_login from '../views/manage/login.vue'

import product_hash_manage from '../views/product_hash_manage.vue'

import { check_lang_key } from '@/data/langs'
let url_lang = check_lang_key();
url_lang = ''

const router_link = [
    {
      path: '',
      redirect: '/product_hash_manage/',
    },
    {
        path: url_lang + '/admin_index',
        name: 'admin_index',
        component: admin_index
    },
    {
        path: url_lang + '/product_hash_manage/:recent_product_id?',
        name: 'product_hash_manage',
        component: product_hash_manage
    },
    {
        path: url_lang + '/product_sku_manage/:goods_id/:goods_name/:goods_code',
        name: 'product_sku_manage',
        component: admin_product_sku_manage
    },
    {
        path: url_lang + '/login',
        name: 'login',
        component: login
    },
    // manage
    {
        path: url_lang + '/manage_admin',
        name: 'manage_admin',
        component: manage_admin
    },
    {
        path: url_lang + '/manage_admin_login',
        name: 'manage_admin_login',
        component: manage_admin_login
    },
    // cusotmer
    {
        path: url_lang + '/product_router/:sku_id',
        name: 'product_router',
        component: product_router
    },
    {
        path: url_lang + '/product_router_history/:sku_id',
        name: 'product_router_history',
        component: product_router_history
    },
    {
        path: url_lang + '/product_cert/:sku_id',
        name: 'product_cert',
        component: product_cert
    },
    {
        path: url_lang + '/report/:sku_id',
        name: 'report',
        component: report
    },
];

vue.directive('main-scroll-forbidden', {
  inserted (el, binding, vnode) {
    document.documentElement.style.cssText ="overflow:hidden";
    let l = document.getElementsByClassName('list_coutomer')[0] || null;
    l ? l.style.position = 'relative' : '';
    l ? l.style.zIndex = '2' : '';
    let r = document.getElementsByClassName('right_main_container')[0] || null;
    r ? r.style.position = 'relative' : '';
    r ? r.style.zIndex = '2' : '';
  },
  unbind () {
    document.documentElement.style.cssText ="overflow:auto";

    let l = document.getElementsByClassName('list_coutomer')[0] || null;
    l ? l.style.position = '' : '';
    l ? l.style.zIndex = '' : '';
    let r = document.getElementsByClassName('right_main_container')[0] || null;
    r ? r.style.position = '' : '';
    r ? r.style.zIndex = '' : '';
  }
});

const router_config = new vue_router({
    // mode: 'history',
    routes: router_link
});

export const app = new vue({
    router: router_config,
    mounted () {
      document.dispatchEvent(new Event('render-event'))
    }
}).$mount('#app');


