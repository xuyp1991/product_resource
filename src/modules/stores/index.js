
export const update_token = (key = 'token', token) => {
  localStorage[key] = token;
  return token;
}

export const get_token = (key = 'token') => {
  return localStorage[key];
}

export const remove_key = (key = '') => {
  delete localStorage[key]
}