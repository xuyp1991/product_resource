export const wait_time = async (_t = 1) => {
  return new Promise((rs, rj) => {
    setTimeout(() => {
      rs();
    }, _t * 1000);
  });
}

export const split_long_num = (num) => {
    if(num === null) return '';
    let num_str = num + '';
    let [int_num, float_num = ''] = num_str.split('.');
    let num_arr = int_num.split('');
    let n = 0, n_arr = []; 
    for(let i of num_arr.reverse()){
        if(!(n%3) && n){n_arr.push(',');}
        n_arr.push(i);
        n ++;
    };
    return n_arr.reverse().join('') + (float_num ? '.' + float_num : '');
}

export const fix_str_len = (words, len = 2, replace_char = '0') => {
  words = (words + '').split('');
  let new_space_length = words.length - len,
      new_space = new_space_length < 0 ? [...new Array(-new_space_length)] : []

  new_space.forEach((i, index) => {
    new_space[index] = replace_char;
  });
  words.splice(0, 0, ...new_space)
  return words.join('');
}

// fix_str_len('hello', 10, '0')
export const random_words = (len = 10) => {
  let asics_sart = 33, 
      asics_end = 127, 
      asics_len = asics_end - asics_sart,
      default_letters = [...new Array(asics_len)];

  default_letters.forEach((item, index) => {
    default_letters[index] = String.fromCharCode(asics_sart + index)
  });

  let result = [];
  for(let res_word in [...new Array(len)]){
    let random_index = parseInt( Math.random() * 1000000 % default_letters.length );
    result.push(default_letters[random_index]);
  }

  return result.join('');
}

import shajs from 'sha.js'
export const sha256 = async (message) => {
  let hash = shajs('sha256').update(message).digest('hex');
  return hash;
}

export const format_date_str = (date = new Date(), formatType = 'YYYY-MM-DD HH:mm:ss') => {
    const filters = [
        {
            keyWord: 'YYYY',
            len: 4,
            method() {
                return Date.prototype.getFullYear.apply(date);
            }
        },
        {
            keyWord: 'MM',
            len: 2,
            method() {
                return Date.prototype.getMonth.apply(date) + 1;
            }
        },
        {
            keyWord: 'DD',
            len: 2,
            method() {
                return Date.prototype.getDate.apply(date);
            }
        },
        {
            keyWord: 'hh',
            len: 2,
            method() {
                return Date.prototype.getHours.apply(date);
            }
        },
        {
            keyWord: 'HH',
            len: 2,
            method() {
                return Date.prototype.getHours.apply(date);
            }
        },
        {
            keyWord: 'mm',
            len: 2,
            method() {
                return Date.prototype.getMinutes.apply(date);
            }
        },
        {
            keyWord: 'ss',
            len: 2,
            method() {
                return Date.prototype.getSeconds.apply(date);
            }
        }
    ];
    let result = formatType;
    filters.forEach((item, index) => {
        const reg = new RegExp(item.keyWord, 'g');
        let DateProperty = item.method();
        DateProperty = fix_str_len(DateProperty, item.len, '0');
        result = result.replace(reg, DateProperty);
    });
    return result;
};
