const mode = process.env.NODE_ENV == 'production' ? 'production' : 'development';

const ExtractTextPlugin = require('extract-text-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const HtmlPlugin = require('html-webpack-plugin');
const path = require('path');

const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
  mode: mode,
  entry: {
    index: './src/controller/index.js'
  },
  output: {
    publicPath: '/s/',
    path: path.resolve(__dirname, 's'),
    filename: '[name][hash].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
                loader: 'css-loader',
                options: {
                    url: true,
                    // minimize: true,
                    sourceMap: false
                }
            }
        ]
        })
      },
      {
        test: /\.html$/,
        use: 'vue-html-loader'
      },

      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      // {
      //   test: /.tsx?$/,
      //   exclude: /node_modules/,
      //   loader: 'ts-loader',
      //   options: {
      //     appendTsSuffixTo: [/\.vue$/],
      //   },
      // },
      // {
      //   test: /\.vue$/,
      //   exclude: /node_modules/,
      //   loader: 'vue-loader',
      // },
      // {
      //   test: /.tsx?$/,
      //   exclude: /node_modules/,
      //   loader: 'ts-loader',
      //   options: {
      //     appendTsSuffixTo: [/\.vue$/],
      //   },
      // },
      {
        test: /\.vue$/,
        use: {
          loader: 'vue-loader',
          options: {
            loaders: {
              sass: 'vue-style-loader!css-loader?indentedSyntax=1',
              scss: 'vue-style-loader!css-loader',
              css: 'vue-style-loader!css-loader'
            }
          }
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          query: {
            limit: 1,
            name: '[name].[ext]'
          }
        }
      },

      {
        test: /\.(ico)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          query: {
            limit: 100,
            name: '[name].[ext]'
          }
        }
      },

      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 1,
          name: 'media/[name]--[folder].[ext]'
        }
      },

      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          query: {
            limit: 10000,
            name: 'fonts/[name]--[folder].[ext]'
          }
        }
      }

    ]
  },
  plugins: [
    new HtmlPlugin({
      chunks: ['index'],
      template: path.resolve(__dirname, 'public/index.html'),
      inject: true,
      filename: 'index.html'
    }),
    new UglifyJsPlugin({
      test: /\.js($|\?)/i,
      uglifyOptions: {
        test: /\.js($|\?)/i,
        compress: true,
        output: {
          comments: false,
          beautify: true
        }
      }
    }),
    new ExtractTextPlugin('[name].[hash].css', {
        disable: false,
        allChunks: true,
    }),
    new VueLoaderPlugin(),
  ],
  devServer: {
    // https://api.smmous.com/
    proxy: { 
      '/product_hash': 'http://0.0.0.0:23901',
      '/v1': 'http://192.168.1.146:22222'
    },
    contentBase: path.join(__dirname, 'dist'), 
    hot: true, 
    host: '0.0.0.0',
    port: 1209,
    open: true
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.vue', '.json'],
    modules: [
      path.resolve(__dirname, "app"), "node_modules"
    ],
    alias: {
      vue: 'vue/dist/vue.min.js',
      '@': path.join(__dirname, './src'),
    },
  }
}