FROM ubuntu:latest
RUN apt-get upgrade -y
RUN apt-get update -y
RUN apt-get install nginx -y

WORKDIR /opt/
ADD ./dist /opt/s
ADD nginx/nginx.conf /etc/nginx/nginx.conf
ADD start_in_docker.sh /opt/

